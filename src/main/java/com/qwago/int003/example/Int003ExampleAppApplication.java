package com.qwago.int003.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"com.qwago.int003"})
public class Int003ExampleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Int003ExampleAppApplication.class, args);
	}

}
