package com.qwago.int003.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.qwago.int003.model.GrossIncomeContext;
import com.qwago.int003.model.NetPayResult;
import com.qwago.int003.utility.TemplateNetPayCalculator;

@Service
public class NetPayCalculatorService {
	
	
	@Autowired
	@Qualifier("com.qwago.int003.DeductionCalculator")
	private TemplateNetPayCalculator templateNetPayCalculator;
	
	
	public NetPayResult performNetPayCalculation(final double grossIncomeSalary) {
		
		final var grossIncomeContext = new GrossIncomeContext();
		grossIncomeContext.setGrossIncome(grossIncomeSalary);
		return this.templateNetPayCalculator.doCalculate(grossIncomeContext);
		
	}

}
