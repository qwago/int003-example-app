package com.qwago.int003.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeductionResult {
	
	private double totalDeduction;
	private String remakrs;
	
	public DeductionResult (double totalDeduction, String remarks) {
		this.totalDeduction = totalDeduction;
		this.remakrs = remarks;
	}

}
