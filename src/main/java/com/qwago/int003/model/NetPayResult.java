package com.qwago.int003.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;

@Getter
@Setter
@AllArgsConstructor
public class NetPayResult {
	
	private double totalNetPay;
	private List<String> remarks = new ArrayList<>();
	
	public NetPayResult (final GrossIncomeContext grossIncomeContext, final double totalDeduction, List<String> remarks ) {
		this.totalNetPay = (grossIncomeContext.getGrossIncome() - totalDeduction);
		this.remarks.addAll(remarks);
	}
	

}
