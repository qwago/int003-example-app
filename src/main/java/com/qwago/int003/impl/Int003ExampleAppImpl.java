package com.qwago.int003.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.qwago.int003.model.NetPayResult;
import com.qwago.int003.service.NetPayCalculatorService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Int003ExampleAppImpl {
	
	@Autowired
	NetPayCalculatorService netPayCalculatorService;
	
	@EventListener(ApplicationReadyEvent.class)
	public void callServices() {
		log.info("Calling Example App Properties ");
		NetPayResult netPayResult = netPayCalculatorService.performNetPayCalculation(500.00);
		log.info("NetPay :" +  netPayResult.getTotalNetPay());
		netPayResult.getRemarks().stream().forEach(System.out::println);
	}
	

}
