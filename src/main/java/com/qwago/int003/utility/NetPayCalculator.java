package com.qwago.int003.utility;

import com.qwago.int003.model.GrossIncomeContext;
import com.qwago.int003.model.NetPayResult;

public abstract class NetPayCalculator {
	
	protected abstract NetPayResult calculate(final GrossIncomeContext grossIncome);

}
