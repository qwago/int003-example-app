package com.qwago.int003.utility;

import com.qwago.int003.model.DeductionResult;
import com.qwago.int003.model.GrossIncomeContext;


public abstract class Calculator {

	protected abstract DeductionResult calculate(final GrossIncomeContext grossIncomeContext);
}
