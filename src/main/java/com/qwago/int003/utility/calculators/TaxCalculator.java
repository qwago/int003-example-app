package com.qwago.int003.utility.calculators;

import com.qwago.int003.model.DeductionResult;
import com.qwago.int003.model.GrossIncomeContext;
import com.qwago.int003.utility.Calculator;

public class TaxCalculator  extends Calculator {

	@Override
	protected DeductionResult calculate(GrossIncomeContext grossIncomeContext) {
		if (grossIncomeContext.getGrossIncome() >= 301 && grossIncomeContext.getGrossIncome() <= 700) {
			return new DeductionResult(calculateDeduction(0.1, grossIncomeContext), "10% Percent Tax Dedecution");
		} else if (grossIncomeContext.getGrossIncome() >= 701 && grossIncomeContext.getGrossIncome() <= 1000) {
			return new DeductionResult(calculateDeduction(0.20, grossIncomeContext), "20% Percent Tax Dedecution");
		} else if (grossIncomeContext.getGrossIncome() >= 1001) {
			return new DeductionResult(calculateDeduction(0.32, grossIncomeContext), "32% Percent Tax Dedecution");
		}
		return new DeductionResult(grossIncomeContext.getGrossIncome(), "No Tax Dedecution");
	}
		
	private double calculateDeduction(double percentage, GrossIncomeContext grossIncomeContext) {
		return percentage * grossIncomeContext.getGrossIncome();
	}

}
