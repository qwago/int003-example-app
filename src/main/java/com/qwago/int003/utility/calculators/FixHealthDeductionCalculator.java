package com.qwago.int003.utility.calculators;

import com.qwago.int003.model.DeductionResult;
import com.qwago.int003.model.GrossIncomeContext;
import com.qwago.int003.utility.Calculator;

public class FixHealthDeductionCalculator extends Calculator{

	@Override
	protected DeductionResult calculate(GrossIncomeContext grossIncomeContext) {
		return new DeductionResult(10, "$10 Fixes Health Deduction ");
	}

}
