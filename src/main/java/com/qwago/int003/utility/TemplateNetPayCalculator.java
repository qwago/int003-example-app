package com.qwago.int003.utility;

import java.util.ArrayList;
import java.util.List;

import com.qwago.int003.model.GrossIncomeContext;
import com.qwago.int003.model.NetPayResult;
import com.qwago.int003.model.DeductionResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateNetPayCalculator {
	
	private List<Calculator> calculators = new ArrayList<>();
	
	
	public NetPayResult doCalculate(final GrossIncomeContext grossIncomcontext) {
		double totalDeduction = 0.00;
		List<String> deductionRemarks = new ArrayList<>();
		for(final Calculator calculator: calculators) {
			log.info("Using " + calculator.getClass().getName());
			DeductionResult deductionResult = calculator.calculate(grossIncomcontext);
			log.info("Deduction : " + deductionResult.getTotalDeduction());
			totalDeduction += deductionResult.getTotalDeduction();
			log.info("Total Deduction : " + totalDeduction);
			deductionRemarks.add(deductionResult.getRemakrs());
		}
				
		return new NetPayResult(grossIncomcontext, totalDeduction, deductionRemarks);
	}
	
	public void add(Calculator calculator) {
		calculators.add(calculator);
	}
	
	public void addAll(List<Calculator> calculators) {
		this.calculators.addAll(calculators);
	}

}
