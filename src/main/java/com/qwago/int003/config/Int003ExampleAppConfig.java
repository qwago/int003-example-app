package com.qwago.int003.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qwago.int003.utility.TemplateNetPayCalculator;
import com.qwago.int003.utility.calculators.FixHealthDeductionCalculator;
import com.qwago.int003.utility.calculators.TaxCalculator;

@Configuration
public class Int003ExampleAppConfig {
	
	
	@Bean(name="com.qwago.int003.DeductionCalculator")
	public TemplateNetPayCalculator createDeductionCalculator() {
		final var calculators = new TemplateNetPayCalculator();
		calculators.add(new TaxCalculator());
		calculators.add(new FixHealthDeductionCalculator());
		return calculators;
	}
}
